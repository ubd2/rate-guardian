UBDToken
========

Some info about UBDT.

Table of Contents
-----------------
* [Contracts](#contracts) 
* [Grabbers](#grabbers)
* [Logger](#logger)
* [Scheduler](#scheduler)
* [Local development](#local-development)
* [Environment](#environment)
* [Useful Links](#useful-links)

Contracts
--------

Every contract implementation inherit [BaseContract](./app/contracts/base_contract.py).

### UBDN Locker Distributor

### Sandbox 1

### Sandbox 2

### Treasury

### TODO:
1. Develop decorator for transactions.

Grabbers
--------

Every grabber implementation inherit [BaseGrabber](./app/grabbers/base_grabber.py).

For now there are 2 grabbers [CoinMarketCap](./app/grabbers/coin_market_cap.py) and [Binance](./app/grabbers/binance.py).

> To use CMC grabber you have to get CMC_KEY on [CoinMarketCap website](https://pro.coinmarketcap.com/account)

### TODO:
1. Refactor BaseGrabber
2. More grabbers


Logger
------

[RateLogger](./app/logger.py) class represent logger.

All logs are sent to group or supergroup via RateBot.

> In Rate Guardian telegram group you can find
information about all bot tokens, chat ids and thread ids

### TODO
1. Refactor logging


Scheduler
--------

To schedule jobs used [APScheduler](https://apscheduler.readthedocs.io/en/3.x/).

### Problems:
1. The way how we change job intervals kinda wierd.
New version of [APScheduler](https://apscheduler.readthedocs.io/en/3.x/) have to fix this problem.

### TODO:
1. migrate to 4.x APScheduler version.
2. Refactor [scheduler.py](./app/scheduler.py)


Local development
-----------------

> Use correct bot and thread for bot logs

For local development copy git repository:
```bash
mkdir RateGuardian
cd RateGuardian
git clone https://gitlab.com/ubd2/rate-guardian.git

```

Build docker image:
```bash
docker build -f ./DockerfileLocal -t rate-guardian:local .
```

Run docker services:
```bash 
docker-compose -p rate-guardian -f docker-compose-local.yaml up
```

Environment
-----------

All public variables located in [.env.task01](./.env.task01)


Private environment variables located in [.env](./.env.example).

`.env` contains:
```dotenv
# HTTP provider
WEB3_PROVIDER=https://goerli.infura.io/v3/xxxxxxxxxxxxxxxxxxxxxxxxxx

# Ethereum wallet private token
ETH_PRIVATE_KEY=

#CoinMarketCap API Key
CMC_KEY=
```


### Scheduling

All environment variables related to scheduling jobs are located in [.env.schedule](./.env.schedule).

To provide less complicated interval setup,
we use custom way of setting intervals:

```dotenv
INTERVAL=1h

ENOTHER_ONE=20s

ONE_MORE=40d

LAST_ONE=15m
```

For now this method support only `(s)econds`, `(m)inutes`, `(h)ours` and `(d)ays`.


### TODO:
1. Support of more complicated intervals, like `1d 2h 3s`.


### Bot

All environment variables related to bot logging are located in [.env.bot](./.env.bot.example).

`.env.bot` contains:

```dotenv
# Bot token
TOKEN=

# Chat, there your logs would be sent
CHAT_ID=

# Supergroups with threads only
THREAD_ID=
```

> In Rate Guardian telegram group you can find
information about all bot tokens, chat ids and thread ids

Useful links
------------

### Contract links

#### Sepolia testnet
- [UBDN Locker Distributor]
- [Sandbox 1]
- [Sandbox 2]
- [Treasury]

#### Ethereum mainnet
- [UBDN Locker Distributor]
- [Sandbox 1](https://etherscan.io/address/0x5eE5c97A594cb4a922B9c0d259bEe7fe0622afAa#code)
- [Sandbox 2](https://etherscan.io/address/0x3ACD1738514A6e123CB9b36b67adE1265109ae62#code)
- [Treasury](https://etherscan.io/address/0x46d81A0E755268FEa738540feE71641227BB69fd#code)
- [UBDToken](https://etherscan.io/address/0x08A0356dF4F4052accE757FaacCEc35b621bBDD2#code)


### Docs
- [APScheduler](https://apscheduler.readthedocs.io/en/3.x/)
- [web3.py](https://web3py.readthedocs.io/en/stable/)
- [requests](https://requests.readthedocs.io/en/latest/)

### Misc
- [CoinMarketCap API](https://coinmarketcap.com/api/documentation/v1/)
- [Binance API](https://www.binance.com/en/binance-api)