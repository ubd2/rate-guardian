from .base_contract import BaseContract
from .types import ContractCredentials

from hexbytes import HexBytes
from web3.exceptions import ContractLogicError
from web3.types import TxParams

from logger import RateLogger


class SandBox1Contact(BaseContract):
    def __init__(self, credentials: ContractCredentials,
                 logger: RateLogger,
                 in_asset: str,
                 in_amount: int,
                 deadline: int,
                 amount_out_min: int,
                 module_address: HexBytes,
                 erc20_abi: str,
                 max_priority_fee_per_gas: int = 0) -> None:
        super().__init__(credentials, logger)

        self.__in_asset = in_asset
        self.__in_amount = in_amount
        self.__deadline = deadline
        self.__amount_out_min = amount_out_min
        self.__module_address = module_address
        self.__erc20_abi = erc20_abi

        self.max_priority_fee_per_gas = max_priority_fee_per_gas

    def send_transaction(self) -> bool:
        params = TxParams({
            'chainId': self.chain_id,
            'from': self.account.address,
            'maxFeePerGas': self._max_fee_per_gas(self.max_priority_fee_per_gas),
            'maxPriorityFeePerGas': self.web3.to_wei(self.max_priority_fee_per_gas, 'gwei'),
            'nonce': self.web3.eth.get_transaction_count(self.account.address),
            'to': self.address
        })

        debug_message = 'topupTreasury() in SandBox1.\n'
        try:
            self.contract.functions.topupTreasury().call()
        except Exception as e:
            debug_message += f'<b>Possible error message:</b> <code>{e.args[0]}</code>\n'

        params_str = "\n".join([f"{k}: <code>{v}</code>" for k, v in params.items()])
        debug_message += f'<b>Transaction parameters:</b>\n{params_str}'
        self.logger.debug(debug_message, True)

        try:
            tx_hash = self.contract.functions.topupTreasury().transact(params)
            self.web3.eth.wait_for_transaction_receipt(tx_hash)
            self.logger.info(f'Sent topupTreasury() method in SandBox1. {self._tx_hash_url(tx_hash.hex())}', True)
            return True
        except ValueError as e:
            data = e.args[0]
            if data['code'] == -32000:
                self.logger.error(f'Tried to sent topupTreasury() in SandBox1, but caught error\n'
                                  f'<b>Code</b>: <code>{data["code"]}</code>\n'
                                  f'<b>Message</b>: <code>{data["message"]}</code>\n'
                                  f'<i>Most likely your wallet has run out of cryptocurrency</i>', True)
            return False
        except ContractLogicError as e:
            self.logger.warning(f'Reverted topupTreasury() in SandBox1'
                                f'<b>Code</b>: <code>{e.data}</code>\n'
                                f'<b>Message</b>: <code>{e.message}</code>\n', True)
        return False

    def __check_balance(self) -> int:
        in_asset = self.web3.eth.contract(address=self.__in_asset, abi=self.__erc20_abi)
        amount = in_asset.functions.balanceOf(self.__module_address).call()
        self.logger.debug(f'Check balance of <code>{self.__module_address}</code>. <b>Amount:</b> {int(amount)}', True)
        return int(amount)

    def swap_exact_input(self):
        balance = self.__check_balance()

        if balance >= self.__in_amount:

            params = TxParams({
                'chainId': self.chain_id,
                'from': self.account.address,
                'maxFeePerGas': self._max_fee_per_gas(self.max_priority_fee_per_gas),
                'maxPriorityFeePerGas': self.web3.to_wei(self.max_priority_fee_per_gas, 'gwei'),
                'nonce': self.web3.eth.get_transaction_count(self.account.address),
                'to': self.address
            })

            call_params = {
                'chainId': self.chain_id,
                'from': self.account.address,
            }

            debug_message = 'swapExactInput() in SandBox1.\n'
            try:
                self.contract.functions.swapExactInput(
                    self.__in_asset,
                    self.__in_amount,
                    self.__deadline,
                    self.__amount_out_min
                ).call(call_params)
            except Exception as e:
                debug_message += f'<b>Possible error message:</b> <code>{e.args[0]}</code>\n'

            params_str = "\n".join([f"{k}: <code>{v}</code>" for k, v in params.items()])
            debug_message += f'<b>Transaction parameters:</b>\n{params_str}\n'
            debug_message += (f'<b>Arguments:</b>\n'
                              f'_inAsset: <code>{self.__in_asset}\n</code>'
                              f'_inAmount: <code>{self.__in_amount}\n</code>'
                              f'_deadline: <code>{self.__deadline}\n</code>'
                              f'_amountOutMin: <code>{self.__amount_out_min}</code>'
                              )
            self.logger.debug(debug_message, True)

            try:
                tx_hash = self.contract.functions.swapExactInput(
                    self.__in_asset,
                    self.__in_amount,
                    self.__deadline,
                    self.__amount_out_min
                ).transact(params)

                self.web3.eth.wait_for_transaction_receipt(tx_hash)
                self.logger.info(f'Sent swapExactInput() method in SandBox1. {self._tx_hash_url(tx_hash.hex())}', True)
                return True
            except ValueError as e:
                data = e.args[0]
                if data['code'] == -32000:
                    self.logger.error(f'Tried to sent swapExactInput() in SandBox1, but caught error\n'
                                      f'<b>Code</b>: <code>{data["code"]}</code>\n'
                                      f'<b>Message</b>: <code>{data["message"]}</code>\n'
                                      f'<i>Most likely your wallet has run out of cryptocurrency</i>', True)
                return False
            except ContractLogicError as e:
                self.logger.warning(f'Reverted swapExactInput() in SandBox1'
                                    f'<b>Code</b>: <code>{e.data}</code>\n'
                                    f'<b>Message</b>: <code>{e.message}</code>\n', True)
            return False
        else:
            self.logger.debug(f'<b>_inAsset balance</b>(<code>{balance}</code>) is <b>less</b> then <b>_inAmount</b>('
                              f'<code>{self.__in_amount}</code>)', True)
