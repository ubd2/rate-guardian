import config
from logger import RateLogger
from .types import ContractCredentials
from web3.eth import Contract
from web3 import Web3, HTTPProvider
from eth_account.account import LocalAccount
from web3.middleware import construct_sign_and_send_raw_middleware

MULTIPLIER: float = 1.25


class BaseContract:
    tx_base_url = config.EXPLORER_BASE_URL

    def __init__(self, credentials: ContractCredentials, logger: RateLogger):
        self.address = credentials['address']
        self.chain_id = credentials['chain_id']
        self.web3: Web3 = Web3(HTTPProvider(credentials['web3']))
        self.account: LocalAccount = self.web3.eth.account.from_key(credentials['account_key'])
        self.contract: Contract = self.web3.eth.contract(address=credentials['address'], abi=credentials['abi'])

        self.web3.middleware_onion.add(construct_sign_and_send_raw_middleware(self.account))
        self.logger = logger

    def _max_fee_per_gas(self, max_priority_fee_per_gas: int) -> int:
        fee_history = self.web3.eth.fee_history(block_count=1, newest_block='latest')
        return int(fee_history['baseFeePerGas'][1] * MULTIPLIER + self.web3.to_wei(max_priority_fee_per_gas, 'gwei'))

    def _tx_hash_url(self, tx_hash):
        return f'{self.tx_base_url}/tx/{tx_hash}'
