from web3.types import TxParams
from grabbers import CoinMarketCapGrabber
from .types import ContractCredentials
from .base_contract import BaseContract

from logger import RateLogger


class UBDNLockerDistributor(BaseContract):
    def __init__(self, credentials: ContractCredentials,
                 logger: RateLogger,
                 coin_address: str,
                 coin_id: int,
                 base_price: str,
                 threshold_value: int,
                 grabber_api_key: str,
                 max_priority_fee_per_gas: int = 0):
        super().__init__(credentials, logger)

        self.__coin_address = coin_address
        self.__coin_id = coin_id
        self.__base_price = base_price
        self.__threshold_value = threshold_value
        self.__grabber_api_key = grabber_api_key
        self.max_priority_fee_per_gas = max_priority_fee_per_gas

    def __emergency_pause_transaction(self):
        params = TxParams({
            'chainId': self.chain_id,
            'from': self.account.address,
            'maxFeePerGas': self._max_fee_per_gas(self.max_priority_fee_per_gas),
            'maxPriorityFeePerGas': self.web3.to_wei(self.max_priority_fee_per_gas, 'gwei'),
            'nonce': self.web3.eth.get_transaction_count(self.account.address),
            'to': self.address
        })

        debug_message = 'emergencyPause() in UBDNLockerDistributor.\n'
        try:
            self.contract.functions.emergencyPause(self.__coin_address).call()
        except Exception as e:
            debug_message += f'<b>Possible error message:</b> <code>{e.args[0]}</code>\n'

        params_str = "\n".join([f"{k}: <code>{v}</code>" for k, v in params.items()])

        debug_message += f'<b>Transaction parameters:</b>\n{params_str}\n'
        debug_message += (f'<b>Arguments:</b>\n'
                          f'_paymentToken: <code>{self.__coin_address}\n</code>'
                          )
        self.logger.debug(debug_message, True)

        try:
            tx_hash = self.contract.functions.emergencyPause(self.__coin_address).transact(params)
            self.logger.info(f'Sent emergencyPause() in UBDNLockerDistributor. {self._tx_hash_url(tx_hash.hex())}', True)
            return self.web3.eth.wait_for_transaction_receipt(tx_hash)
        except ValueError as e:
            data = e.args[0]
            if data['code'] == -32000:
                self.logger.error(f'Tried to sent emergencyPause() in UBDNLockerDistributor, but caught error\n'
                                  f'<b>Code</b>: <code>{data["code"]}</code>\n'
                                  f'<b>Message</b>: <code>{data["message"]}</code>\n'
                                  f'<i>Most likely your wallet has run out of cryptocurrency</i>', True)
        return None

    def send_transaction(self) -> bool:
        price_grabber = CoinMarketCapGrabber(self.__grabber_api_key)
        symbol_price = price_grabber.get_price(self.__coin_id)
        symbol, price = symbol_price['symbol'], symbol_price['value']

        self.logger.debug('<b>Grabber in UBDNLockerDistributor</b>\n'
                          f'coin_address: <code>{self.__coin_address}</code>\n'
                          f'coin_id: <code>{self.__coin_id}</code>\n'
                          f'base_price: <code>{self.__base_price}</code>\n'
                          f'threshold_value: <code>{self.__threshold_value}</code>\n'
                          , True)

        if abs(price - self.__base_price) > self.__threshold_value:
            self.__emergency_pause_transaction()
            self.logger.info(f'Depeg detected ({symbol} price {price}), job paused.', True)
            return True
        else:
            self.logger.debug(f'Depeg NOT detected ({symbol} price {price})', True)
            return False
