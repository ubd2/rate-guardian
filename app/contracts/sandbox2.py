from .base_contract import BaseContract
from .types import ContractCredentials

from web3.types import TxParams
from web3.exceptions import ContractLogicError

from logger import RateLogger


class SandBox2Contract(BaseContract):
    def __init__(self, credentials: ContractCredentials, logger: RateLogger, max_priority_fee_per_gas: int = 0):
        super().__init__(credentials, logger)

        self.max_priority_fee_per_gas = max_priority_fee_per_gas

    def topup_sandbox2(self) -> bool:
        params = TxParams({
            'chainId': self.chain_id,
            'from': self.account.address,
            'maxFeePerGas': self._max_fee_per_gas(self.max_priority_fee_per_gas),
            'maxPriorityFeePerGas': self.web3.to_wei(self.max_priority_fee_per_gas, 'gwei'),
            'nonce': self.web3.eth.get_transaction_count(self.account.address),
            'to': self.address
        })

        debug_message = 'topupSandBox2() in SandBox2.\n'
        try:
            self.contract.functions.topupSandBox2().call()
        except Exception as e:
            debug_message += f'<b>Possible error message:</b> <code>{e.args[0]}</code>\n'

        params_str = "\n".join([f"{k}: <code>{v}</code>" for k, v in params.items()])
        debug_message += f'<b>Transaction parameters:</b>\n{params_str}'
        self.logger.debug(debug_message, True)

        try:
            tx_hash = self.contract.functions.topupSandBox2().transact(params)
            self.web3.eth.wait_for_transaction_receipt(tx_hash)
            self.logger.info(f'Sent topupSandBox2() in SandBox2. {self._tx_hash_url(tx_hash.hex())}', True)
            return True

        except ValueError as e:
            data = e.args[0]
            if data['code'] == -32000:
                self.logger.error(f'Tried to sent topupSandBox2() in SandBox2, but caught error\n'
                                  f'<b>Code</b>: <code>{data["code"]}</code>\n'
                                  f'<b>Message</b>: <code>{data["message"]}</code>\n'
                                  f'<i>Most likely your wallet has run out of cryptocurrency</i>', True)
            return False

        except ContractLogicError:
            self.logger.warning('Reverted topupSandBox2() in SandBox2', True)
            return False

    def send_transaction(self) -> bool:

        params = TxParams({
            'chainId': self.chain_id,
            'from': self.account.address,
            'maxFeePerGas': self._max_fee_per_gas(self.max_priority_fee_per_gas),
            'maxPriorityFeePerGas': self.web3.to_wei(self.max_priority_fee_per_gas, 'gwei'),
            'nonce': self.web3.eth.get_transaction_count(self.account.address),
            'to': self.address
        })

        debug_message = 'topupTreasury() in Sandbox2.\n'
        try:
            self.contract.functions.topupTreasury().call()
        except Exception as e:
            debug_message += f'<b>Possible error message:</b> <code>{e.args[0]}</code>\n'

        params_str = "\n".join([f"{k}: <code>{v}</code>" for k, v in params.items()])

        debug_message += f'<b>Transaction parameters:</b>\n{params_str}'
        self.logger.debug(debug_message, True)

        try:
            tx_hash = self.contract.functions.topupTreasury().transact(params)
            self.web3.eth.wait_for_transaction_receipt(tx_hash)
            self.logger.info(f'Sent topupTreasury() in Sandbox2. {self._tx_hash_url(tx_hash.hex())}', True)
            return True

        except ValueError as e:
            data = e.args[0]
            if data['code'] == -32000:
                self.logger.error(f'Tried to sent topupTreasury() in SandBox2, but caught error\n'
                                  f'<b>Code</b>: <code>{data["code"]}</code>\n'
                                  f'<b>Message</b>: <code>{data["message"]}</code>\n'
                                  f'<i>Most likely your wallet has run out of cryptocurrency</i>', True)
            return False

        except ContractLogicError as e:
            self.logger.warning(f'<b>Reverted topupTreasury() in SandBox2</b>\n<b>Message:</b> <code>{e.message}</code>', True)
            return False
