from typing import TypedDict
from hexbytes import HexBytes


class ContractCredentials(TypedDict):
    web3: str
    account_key: HexBytes
    address: HexBytes
    abi: str
    chain_id: int
