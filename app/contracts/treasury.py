from .types import ContractCredentials
from .sandbox2 import SandBox2Contract
from .base_contract import BaseContract

from logger import RateLogger


class TreasuryContract(BaseContract):
    def __init__(self, credentials: ContractCredentials, logger: RateLogger, sb2_credentials: ContractCredentials, max_priority_fee_per_gas: int = 0):
        super().__init__(credentials, logger)

        self.__credentials = credentials
        self.__sb2_credentials = sb2_credentials
        self.max_priority_fee_per_gas = max_priority_fee_per_gas

    def __is_ready_for_topup_sandbox2(self) -> bool:
        result = self.contract.functions.isReadyForTopupSandBox2().call()
        self.logger.info(f'Called isReadyForTopupSandBox2() in Treasury\n<b>Value:</b> <code>{result}</code>', True)
        return result

    def send_transaction(self) -> bool:
        if self.__is_ready_for_topup_sandbox2() is False:
            return True

        sb2 = SandBox2Contract(self.__sb2_credentials, RateLogger('Sandbox2', self.logger.bot_credentials), self.max_priority_fee_per_gas)
        return sb2.topup_sandbox2()
