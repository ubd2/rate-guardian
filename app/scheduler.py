import re
import config
from apscheduler.job import Job
from apscheduler.schedulers.blocking import BlockingScheduler

from contracts.sandbox1 import SandBox1Contact
from contracts.types import ContractCredentials
from contracts.treasury import TreasuryContract
from contracts.sandbox2 import SandBox2Contract
from contracts.distributor import UBDNLockerDistributor

from logger import BotCredentials, RateLogger

""" Scheduler """
scheduler = BlockingScheduler(job_defaults={'max_instances': 1, 'coalesce': True})


UBDN_locker_distributor_credentials: ContractCredentials = {
    'web3': config.WEB3_PROVIDER,
    'account_key': config.ETH_PRIVATE_KEY,
    'address': config.UBDN_LOCKER_DISTRIBUTOR_CONTRACT_ADDRESS,
    'abi': config.UBDN_LOCKER_DISTRIBUTOR_CONTRACT_ABI,
    'chain_id': config.CHAIN_ID
}

sandbox1_credentials: ContractCredentials = {
    'web3': config.WEB3_PROVIDER,
    'account_key': config.ETH_PRIVATE_KEY,
    'address': config.SB1_CONTRACT_ADDRESS,
    'abi': config.SB1_CONTRACT_ABI,
    'chain_id': config.CHAIN_ID
}

sandbox2_credentials: ContractCredentials = {
    'web3': config.WEB3_PROVIDER,
    'account_key': config.ETH_PRIVATE_KEY,
    'address': config.SB2_CONTRACT_ADDRESS,
    'abi': config.SB2_CONTRACT_ABI,
    'chain_id': config.CHAIN_ID
}

treasury_credentials: ContractCredentials = {
    'web3': config.WEB3_PROVIDER,
    'account_key': config.ETH_PRIVATE_KEY,
    'address': config.TREASURY_CONTRACT_ADDRESS,
    'abi': config.TREASURY_CONTRACT_ABI,
    'chain_id': config.CHAIN_ID
}

JOBS = {
    'fill_treasury_sandbox1_job': 'sandbox1_job',
    'fill_treasury_sandbox2_job': 'sandbox2_job',
    'UBDN_locker_distributor_job': 'distributor_job',
    'fill_sandbox2': 'treasury_job',
    'swap_exact_input_sandbox1_job': 'sei_sandbox1_job'
}

BOT_CREDENTIALS = BotCredentials(token=config.TOKEN, chat_id=config.CHAT_ID, thread_id=config.THREAD_ID)


def swap_exact_input_sandbox1_job(contract, state):
    if state == 'normal':
        contract.swap_exact_input()


def fill_treasury_sandbox1_job(contract, state):
    if state == 'normal':
        if not contract.send_transaction():
            scheduler.modify_job(job_id='sandbox1_job', kwargs={'contract': contract, 'state': 'reverted'})
            scheduler.reschedule_job(job_id='sandbox1_job', trigger='interval',
                                     **trigger_args(config.SANDBOX1_REVERTED))

    elif state == 'reverted':
        if contract.send_transaction():
            scheduler.modify_job(job_id='sandbox1_job', kwargs={'contract': contract, 'state': 'normal'})
            scheduler.reschedule_job(job_id='sandbox1_job', trigger='interval',
                                     **trigger_args(config.SANDBOX1_NORMAL))


def fill_treasury_sandbox2_job(contract, state):
    if state == 'normal':
        if not contract.send_transaction():
            scheduler.modify_job(job_id='sandbox2_job', kwargs={'contract': contract, 'state': 'reverted'})
            scheduler.reschedule_job(job_id='sandbox2_job', trigger='interval',
                                     **trigger_args(config.SANDBOX2_REVERTED))
    elif state == 'reverted':
        if contract.send_transaction():
            scheduler.modify_job(job_id='sandbox2_job', kwargs={'contract': contract, 'state': 'normal'})
            scheduler.reschedule_job(job_id='sandbox2_job', trigger='interval',
                                     **trigger_args(config.SANDBOX2_NORMAL))


def UBDN_locker_distributor_job(contract, state):
    if state == 'normal':
        if contract.send_transaction():
            scheduler.modify_job(job_id='distributor_job', kwargs={'contract': contract, 'state': 'paused'})
            scheduler.reschedule_job(job_id='distributor_job', trigger='interval',
                                     **trigger_args(config.UBDN_LOCKER_DISTRIBUTOR_PAUSED))

    elif state == 'paused':
        scheduler.modify_job(job_id='distributor_job', kwargs={'contract': contract, 'state': 'normal'})
        scheduler.reschedule_job(job_id='distributor_job', trigger='interval',
                                 **trigger_args(config.UBDN_LOCKER_DISTRIBUTOR_NORMAL))


def fill_sandbox2(contract, state):
    if state == 'normal':
        contract.send_transaction()


def setup_scheduler():
    """"""
    """ UBDNLockerDistributor control contract """
    UBDN_locker_distributor_contract = UBDNLockerDistributor(
        UBDN_locker_distributor_credentials,
        RateLogger('UBDNLockerDistributor', BOT_CREDENTIALS),
        config.COIN_ADDRESS,
        config.COIN_ID,
        config.BASE_PRICE,
        config.THRESHOLD_VALUE,
        config.CMC_KEY,
        config.MAX_PRIORITY_FEE_PER_GAS
    )

    """ Sandbox 1 contract """
    sandbox1_contract = SandBox1Contact(
        sandbox1_credentials,
        RateLogger('Sandbox1', BOT_CREDENTIALS),
        config.IN_ASSET,
        config.IN_AMOUNT,
        config.DEADLINE,
        config.AMOUNT_OUT_MIN,
        config.MODULE_ADDRESS,
        config.ERC20_ABI,
        config.MAX_PRIORITY_FEE_PER_GAS
    )

    """ Sandbox 2 contract """
    sandbox2_contract = SandBox2Contract(
        sandbox2_credentials,
        RateLogger('Sandbox2', BOT_CREDENTIALS),
        config.MAX_PRIORITY_FEE_PER_GAS
    )

    """ Treasury contract """
    treasury_contract = TreasuryContract(
        treasury_credentials,
        RateLogger('Treasury', BOT_CREDENTIALS),
        sandbox2_credentials,
        config.MAX_PRIORITY_FEE_PER_GAS
    )

    """ Jobs """
    logger = RateLogger('Scheduler', BOT_CREDENTIALS)

    if config.UBDN_LOCKER_DISTRIBUTOR_NORMAL is None:
        logger.warning('UBDN_LOCKER_DISTRIBUTOR job is <b>NOT</b> running', True)
    else:
        distributor_job: Job = scheduler.add_job(
            UBDN_locker_distributor_job,
            kwargs={'contract': UBDN_locker_distributor_contract, 'state': 'normal'},
            trigger='interval',
            **trigger_args(config.UBDN_LOCKER_DISTRIBUTOR_NORMAL),
            id='distributor_job'
        )

    if config.SANDBOX1_NORMAL is None:
        logger.warning('Fill treasury SandBox1 job is <b>NOT</b> running', True)
    else:
        sandbox1_job: Job = scheduler.add_job(
            fill_treasury_sandbox1_job,
            kwargs={'contract': sandbox1_contract, 'state': 'normal'},
            trigger='interval',
            **trigger_args(config.SANDBOX1_NORMAL),
            id='sandbox1_job'
        )

    if config.SANDBOX2_NORMAL is None:
        logger.warning('Fill treasury SandBox2 job is <b>NOT</b> running', True)
    else:
        sandbox2_job: Job = scheduler.add_job(
            fill_treasury_sandbox2_job,
            kwargs={'contract': sandbox2_contract, 'state': 'normal'},
            trigger='interval',
            **trigger_args(config.SANDBOX2_NORMAL),
            id='sandbox2_job'
        )

    if config.TREASURY_NORMAL is None:
        logger.warning('Fill sandbox2 Treasury job is <b>NOT</b> running', True)
    else:
        fill_sb2_job: Job = scheduler.add_job(
            fill_sandbox2,
            kwargs={'contract': treasury_contract, 'state': 'normal'},
            trigger='interval',
            **trigger_args(config.TREASURY_NORMAL),
            id='treasury_job'
        )

    if config.SEI_SANDBOX1_NORMAL is None:
        logger.warning('SwapExactInput in sandbox1 job is <b>NOT</b> running', True)
    else:
        sei_sandbox1_job: Job = scheduler.add_job(
            swap_exact_input_sandbox1_job,
            kwargs={'contract': sandbox1_contract, 'state': 'normal'},
            trigger='interval',
            **trigger_args(config.SEI_SANDBOX1_NORMAL),
            id='sei_sandbox1_job'
        )

    return scheduler


def trigger_args(interval: str) -> dict:
    time = {
        's': 'seconds',
        'm': 'minutes',
        'h': 'hours',
        'd': 'days'
    }
    time.keys()
    if re.match("^[0-9]+[smhd]$", interval) is None:
        raise ValueError(f"{interval} doesn't match regex, check README please")
    else:
        return {time[interval[-1]]: int(interval[:-1])}
