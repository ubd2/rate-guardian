import os
import json
import logging

""" Logging """
APP_LOG_LEVEL = int(os.getenv('APP_LOG_LEVEL', logging.DEBUG))

""" Web3 credentials"""
CHAIN_ID = int(os.getenv('CHAIN_ID'))
COIN_ADDRESS = os.getenv('COIN_ADDRESS')
GAS_LIMIT = int(os.getenv('GAS_LIMIT', 700000))
EXPLORER_BASE_URL = os.getenv('EXPLORER_BASE_URL')
MAX_FEE_PER_GAS = int(os.getenv('MAX_FEE_PER_GAS', -1))
MAX_PRIORITY_FEE_PER_GAS = float(os.getenv('MAX_PRIORITY_FEE_PER_GAS', 1))


ETH_PRIVATE_KEY = os.getenv('ETH_PRIVATE_KEY')
WEB3_PROVIDER = os.getenv('WEB3_PROVIDER', 'http://127.0.0.1:8545')

""" UBDNLockerDistributor contract"""
PERIOD = int(os.getenv('PERIOD', 5))
BASE_PRICE = int(os.getenv('BASE_PRICE', 1))
COIN_ID = int(os.getenv('COIN_ID', 3))
THRESHOLD_VALUE = float(os.getenv('THRESHOLD_VALUE', 0))

CMC_KEY = os.getenv('CMC_KEY')

UBDN_LOCKER_DISTRIBUTOR_CONTRACT_ADDRESS = os.getenv('UBDN_LOCKER_DISTRIBUTOR_CONTRACT_ADDRESS')
UBDN_LOCKER_DISTRIBUTOR_ABI_FILEPATH = os.getenv('UBDN_LOCKER_DISTRIBUTOR_ABI_FILEPATH')

with open(UBDN_LOCKER_DISTRIBUTOR_ABI_FILEPATH) as f:
    UBDN_LOCKER_DISTRIBUTOR_CONTRACT_ABI = json.loads(f.read())

""" Sandbox1 contract """
IN_ASSET = os.getenv('IN_ASSET')
IN_AMOUNT = int(float(os.getenv('IN_AMOUNT')))
DEADLINE = int(os.getenv('DEADLINE', 0))
AMOUNT_OUT_MIN = int(os.getenv('AMOUNT_OUT_MIN', 0))

MODULE_ADDRESS = os.getenv('MODULE_ADDRESS')
ERC20_ABI_FILEPATH = os.getenv('ERC20_ABI_FILEPATH')

with open(ERC20_ABI_FILEPATH) as f:
    ERC20_ABI = json.loads(f.read())


SB1_CONTRACT_ADDRESS = os.getenv('SB1_CONTRACT_ADDRESS')
SB1_CONTRACT_ABI_FILEPATH = os.getenv('SB1_CONTRACT_ABI_FILEPATH')

with open(SB1_CONTRACT_ABI_FILEPATH) as f:
    SB1_CONTRACT_ABI = json.loads(f.read())

""" Sandbox2 contract """
SB2_CONTRACT_ADDRESS = os.getenv('SB2_CONTRACT_ADDRESS')
SB2_CONTRACT_ABI_FILEPATH = os.getenv('SB2_CONTRACT_ABI_FILEPATH')

with open(SB2_CONTRACT_ABI_FILEPATH) as f:
    SB2_CONTRACT_ABI = json.loads(f.read())

""" Treasury contract """
TREASURY_CONTRACT_ADDRESS = os.getenv('TREASURY_CONTRACT_ADDRESS')
TREASURY_CONTRACT_ABI_FILEPATH = os.getenv('TREASURY_CONTRACT_ABI_FILEPATH')

with open(TREASURY_CONTRACT_ABI_FILEPATH) as f:
    TREASURY_CONTRACT_ABI = json.loads(f.read())

""" Bot credentials """
TOKEN = os.getenv('TOKEN')
CHAT_ID = int(os.getenv('CHAT_ID'))
THREAD_ID = os.getenv('THREAD_ID', None)

""" Scheduler """
UBDN_LOCKER_DISTRIBUTOR_NORMAL = os.getenv('UBDN_LOCKER_DISTRIBUTOR_NORMAL')
UBDN_LOCKER_DISTRIBUTOR_PAUSED = os.getenv('UBDN_LOCKER_DISTRIBUTOR_PAUSED')

SANDBOX1_NORMAL = os.getenv('SANDBOX1_NORMAL')
SANDBOX1_REVERTED = os.getenv('SANDBOX1_REVERTED')

SANDBOX2_NORMAL = os.getenv('SANDBOX2_NORMAL')
SANDBOX2_REVERTED = os.getenv('SANDBOX2_REVERTED')

TREASURY_NORMAL = os.getenv('TREASURY_NORMAL')

SEI_SANDBOX1_NORMAL = os.getenv('SEI_SANDBOX1_NORMAL')
