from typing import TypedDict


class SymbolPrice(TypedDict):
    symbol: str
    value: float
