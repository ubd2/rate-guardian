from abc import ABC, abstractmethod
from .types import SymbolPrice


class BaseGrabber(ABC):
    @property
    @abstractmethod
    def client(self):
        pass

    @abstractmethod
    def get_price(self, symbol: str) -> SymbolPrice:
        pass
