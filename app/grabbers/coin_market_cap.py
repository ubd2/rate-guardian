import json
import logging
import requests

from .base_grabber import BaseGrabber
from .types import SymbolPrice

logger = logging.getLogger(__name__)


class CoinMarketCapGrabber(BaseGrabber):
    def __init__(self, api_key):
        self.__api_key = api_key

    @property
    def client(self):
        return

    def get_price(self, id: int):
        url = 'https://pro-api.coinmarketcap.com/v2/cryptocurrency/quotes/latest'
        payload = {
            'id': id,
            'convert': 'USD'
        }
        headers = {
            'Accepts': 'application/json',
            'X-CMC_PRO_API_KEY': self.__api_key,
        }

        response = requests.get(url, params=payload, headers=headers)
        data = json.loads(response.text)['data'][str(id)]
        symbol_price: SymbolPrice = {'symbol': data['symbol'], 'value': float(data['quote']['USD']['price'])}
        logger.debug('Received price of %s, price: %f' % (symbol_price['symbol'], symbol_price['value']))
        return symbol_price
