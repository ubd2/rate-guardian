import logging
from binance.client import Client

from .base_grabber import BaseGrabber
from .types import SymbolPrice

logger = logging.getLogger(__name__)


class BinanceGrabber(BaseGrabber):
    @property
    def client(self):
        return Client()

    def get_price(self, symbol: str) -> SymbolPrice:
        request = self.client.get_symbol_ticker(symbol=symbol)
        symbol_price: SymbolPrice = {'symbol': request['symbol'], 'value': float(request['price'])}
        logger.debug('Received price of %s, price: %f' % (symbol_price['symbol'], symbol_price['value']))
        return symbol_price
