import logging
import requests
from enum import IntEnum
from typing import TypedDict


class LogLevel(IntEnum):
    CRITICAL = 50
    FATAL = CRITICAL
    ERROR = 40
    WARNING = 30
    WARN = WARNING
    INFO = 20
    DEBUG = 10
    NOTSET = 0


class BotCredentials(TypedDict):
    token: str
    chat_id: int
    thread_id: int | None


class RateLogger:
    def __init__(self, logger_name: str, bot_credentials: BotCredentials) -> None:
        self.bot_credentials = bot_credentials
        self.logger = logging.getLogger(logger_name)
        self.parse_mode = 'HTML'

    def send_message(self, text: str, level: str | None = None):
        levels = {
            'warning': '<b>⚠️Warning:</b>',
            'info': '<b>📃Info:</b>',
            'error': '<b>🚫Error:</b>',
            'debug': '<b>🐛Debug:</b>'
        }
        lvl: str | None = levels.get(level)
        if lvl is not None:
            text = f'{lvl} {text}'

        token = self.bot_credentials['token']
        chat_id = self.bot_credentials['chat_id']
        thread_id = self.bot_credentials['thread_id']

        payload = {'chat_id': chat_id, 'message_thread_id': thread_id, 'text': text, 'parse_mode': self.parse_mode}
        url_req = f'https://api.telegram.org/bot{token}/sendMessage'
        results = requests.get(url_req, params=payload)

    def info(self, text: str, to_chat: bool = False):
        self.logger.info(remove_markdown(text))
        if to_chat and (self.logger.getEffectiveLevel() <= LogLevel.INFO):
            self.send_message(text, 'info')

    def warning(self, text: str, to_chat: bool = False):
        self.logger.warning(remove_markdown(text))
        if to_chat and (self.logger.getEffectiveLevel() <= LogLevel.WARNING):
            self.send_message(text, 'warning')

    def error(self, text: str, to_chat: bool = False):
        self.logger.error(remove_markdown(text))
        if to_chat and (self.logger.getEffectiveLevel() <= LogLevel.ERROR):
            self.send_message(text, 'error')

    def debug(self, text: str, to_chat: bool = False):
        self.logger.debug(remove_markdown(text))
        if to_chat and (self.logger.getEffectiveLevel() <= LogLevel.DEBUG):
            self.send_message(text, 'debug')


def setup_logging(name: str, bot_credentials: BotCredentials, log_level: LogLevel = logging.DEBUG):
    """Setup root logger and quiet some levels."""

    # Set log format to display the logger name to hunt down verbose logging modules
    fmt = "%(asctime)s->%(levelname)s:[in %(name)s.py:%(lineno)d]:%(message)s"
    logging.basicConfig(format=fmt, level=log_level)

    # Disable logging of JSON-RPC requests and replies
    logging.getLogger("web3.RequestManager").setLevel(logging.WARNING)
    logging.getLogger("web3.providers.HTTPProvider").setLevel(logging.WARNING)
    # logging.getLogger("web3.RequestManager").propagate = False

    # Disable scheduler logging
    logging.getLogger("apscheduler").setLevel(logging.WARNING)

    # Disable all internal debug logging of requests and urllib3
    # E.g. HTTP traffic
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)

    return RateLogger(name, bot_credentials)


def remove_markdown(text: str) -> str:
    signs = ('b', 'i', 'code')
    for sign in signs:
        text = text.replace(f'<{sign}>', '')
        text = text.replace(f'</{sign}>', '')
    return text
