import config
from logger import setup_logging, BotCredentials, LogLevel
from scheduler import setup_scheduler

BOT_CREDENTIALS = BotCredentials(token=config.TOKEN, chat_id=config.CHAT_ID, thread_id=config.THREAD_ID)

if __name__ == '__main__':
    """ Logger """
    logger = setup_logging(name='main', bot_credentials=BOT_CREDENTIALS, log_level=config.APP_LOG_LEVEL)
    logger.info('Rate Guardian started!', True)
    logger.debug(f'log_level: <code>{config.APP_LOG_LEVEL}</code>\n'
                 f''
                 , True)

    """ Scheduler """
    scheduler = setup_scheduler()

    try:
        scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        scheduler.shutdown()
        logger.info('Rate Guardian stopped!', True)
    else:
        logger.error('WHOPS!!!! SOMETHING WENT WRONG', True)
