import pytest

from app.grabbers.binance import BinanceGrabber


@pytest.fixture()
def grabber():
    return BinanceGrabber()


def test_get_price(grabber):
    price = grabber.get_price("BUSDUSDT")
    assert isinstance(price, float)
