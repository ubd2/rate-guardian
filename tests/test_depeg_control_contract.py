import pytest

from web3 import (
    EthereumTesterProvider,
    Web3,
)


@pytest.fixture
def tester_provider():
    return EthereumTesterProvider()


@pytest.fixture
def eth_tester(tester_provider):
    return tester_provider.ethereum_tester


@pytest.fixture
def w3(tester_provider):
    return Web3(tester_provider)


@pytest.fixture
def foo_contract(eth_tester, w3):

    deploy_address = eth_tester.get_accounts()[0]
    acct = w3.eth.accounts[1]
    contract = w3.eth.contract(
        bytecode='''608060405261025860005534801561001657600080fd5b506101c0806100266000396000f3fe608060405234801561001057600080fd5b506004361061004c5760003560e01c806301fa3e1f146100515780637041b48a14610066578063b786896214610079578063c3b88b4214610094575b600080fd5b61006461005f36600461011a565b600055565b005b610064610074366004610133565b6100b4565b61008260005481565b60405190815260200160405180910390f35b6100826100a2366004610133565b60016020526000908152604090205481565b6000546100c19042610163565b6001600160a01b038216600081815260016020526040908190208390555190917f54eab57be6a72992bf271e2d488afa40cc41d14ae46c1fe9a7419aa0379837449161010f91815260200190565b60405180910390a250565b60006020828403121561012c57600080fd5b5035919050565b60006020828403121561014557600080fd5b81356001600160a01b038116811461015c57600080fd5b9392505050565b8082018082111561018457634e487b7160e01b600052601160045260246000fd5b9291505056fea2646970667358221220db5d7cf7c14d7042bae5f8868d34881944fc59e38101c5977201c93d7d19b1a464736f6c63430008150033''',
        abi="""[{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"Token",
        "type":"address"},{"indexed":false,"internalType":"uint256","name":"Until","type":"uint256"}],
        "name":"PaymentTokenPaused","type":"event"},{"inputs":[],"name":"EMERGENCY_PAYMENT_PAUSE","outputs":[{
        "internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},
        {"inputs":[{"internalType":"address","name":"_paymentToken","type":"address"}],"name":"emergencyPause",
        "outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address",
        "name":"","type":"address"}],"name":"paymentTokens","outputs":[{"internalType":"uint256","name":"",
        "type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256",
        "name":"period","type":"uint256"}],"name":"setEMERGENCY_PAYMENT_PAUSE","outputs":[],
        "stateMutability":"nonpayable","type":"function"}]"""
    )

    tx_hash = contract.constructor().transact(
        {
            "from": deploy_address,
        }
    )
    # wait for the transaction to be mined
    tx_receipt = w3.eth.wait_for_transaction_receipt(tx_hash, 180)
    # instantiate and return an instance of our contract.
    return contract(tx_receipt.contractAddress)
