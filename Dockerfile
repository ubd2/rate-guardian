FROM python:3.11

# Set the working directory to /app
WORKDIR /app

# Install any needed packages specified in requirements.txt
ADD ./requirements.txt /app
RUN pip install --trusted-host pypi.python.org -r requirements.txt

COPY ./app /app
